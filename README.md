# LittleVGL integration to FIORD

Sample application demonstrating usage of LittleVGL on ESP32. 

Initial driver code made in the `esp_display` repository then integrated 
with LittleVGL [https://littlevgl.com/].