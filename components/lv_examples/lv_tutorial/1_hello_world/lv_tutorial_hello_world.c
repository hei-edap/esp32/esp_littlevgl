/**
 * @file lv_tutorial_hello_world
 *
 */

/*
 *-------------------------------------------------------------------------------
 * Create your first object: a "Hello world" label
 * ------------------------------------------------------------------------------
 *
 * If you have ported the LittlevGL you are ready to create content on your display.
 *
 * Now you will create a "Hello world!" label and align it to the middle.
 */

/*********************
 *      INCLUDES
 *********************/
#include "lv_tutorial_hello_world.h"

#if LV_USE_TUTORIALS

/*********************
 *      DEFINES
 *********************/

/**********************
 *      TYPEDEFS
 **********************/

/**********************
 *  STATIC PROTOTYPES
 **********************/

/**********************
 *  STATIC VARIABLES
 **********************/

/**********************
 *      MACROS
 **********************/

/**********************
 *   GLOBAL FUNCTIONS
 **********************/

/**
 * Create a simple 'Hello world!' label
 */
void lv_tutorial_hello_world(void)
{
    lv_obj_t * scr = lv_disp_get_scr_act(NULL);     /*Get the current screen*/

    /*Create a Label on the currently active screen*/
    lv_obj_t * label1 =  lv_label_create(scr, NULL);

    /*Modify the Label's text*/
    lv_label_set_text(label1, "Hello world!");

    /* Align the Label to the center
     * NULL means align on parent (which is the screen now)
     * 0, 0 at the end means an x, y offset after alignment*/
    lv_obj_align(label1, NULL, LV_ALIGN_CENTER, 0, 0);

//    /*Create an array for the points of the line*/
//    static lv_point_t line_points[] = { {5, 5}, {70, 70}, {120, 10}, {180, 60}, {240, 10} };
//
//    /*Create new style (thick dark blue)*/
//    static lv_style_t style_line;
//    lv_style_copy(&style_line, &lv_style_plain);
//    style_line.line.color = LV_COLOR_MAKE(0x00, 0x3b, 0x75);
//    style_line.line.width = 3;
//    style_line.line.rounded = 1;
//
//    /*Copy the previous line and apply the new style*/
//    lv_obj_t * line1;
//    line1 = lv_line_create(lv_scr_act(), NULL);
//    lv_line_set_points(line1, line_points, 5);     /*Set the points*/
//    lv_line_set_style(line1, LV_LINE_STYLE_MAIN, &style_line);
//    lv_obj_align(line1, NULL, LV_ALIGN_CENTER, 0, 0);
}

/**********************
 *   STATIC FUNCTIONS
 **********************/

#endif /*LV_USE_TUTORIALS*/
