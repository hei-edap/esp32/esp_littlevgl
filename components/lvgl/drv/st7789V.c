/*********************
 *      INCLUDES
 *********************/
#include "st7789V.h"
#include "driver/gpio.h"
#include "driver/spi_master.h"
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"

const char *TAG = "ST7789V driver";

/*********************
 *      DEFINES
 *********************/

/**********************
 *      TYPEDEFS
 **********************/
/*
 Command and arguments to begin with during screen init
*/
typedef struct {
    uint8_t cmd;
    uint8_t data[16];
    uint8_t databytes; //No of data in data; bit 7 = delay after set; 0xFF = end of cmds.
} st7789v_init_cmd;

/**********************
 *  STATIC PROTOTYPES
 ***********************/
static void st7789v_send_cmd(const uint8_t cmd);

static void st7789v_send_data(const uint8_t *data, int len);

static void st7789v_send_color(void *data, uint16_t length);

static void IRAM_ATTR
spi_ready(spi_transaction_t
*trans);

/**********************
 *  STATIC VARIABLES
 **********************/
bool isDisplayOn = false;
spi_device_handle_t spi8 = NULL;

static volatile bool spi_trans_in_progress = false;
static volatile bool spi_color_sent = false;

spi_bus_config_t buscfg = {
        .miso_io_num= PIN_NUM_MISO,
        .mosi_io_num= PIN_NUM_MOSI,
        .sclk_io_num= PIN_NUM_CLK,
        .max_transfer_sz = N_LINES_SENT * 240 * 18,
        .quadwp_io_num= -1,
        .quadhd_io_num= -1
};

spi_device_interface_config_t devcfg8bits = {
        .address_bits=0,
        .clock_speed_hz= SPI_SPEED, //Clock out at 40 MHz. 80 MHz does not work well with long wires...
        .flags = SPI_DEVICE_HALFDUPLEX, // Required to have speed > 26 MHz
        .pre_cb= NULL,
        .post_cb= spi_ready,
        .mode= 0,                        //SPI mode 0
        .spics_io_num= PIN_NUM_CS,       //CS pin
        .queue_size= 4                   //We want to be able to queue 4 transactions
};

// Commands to be sent when initializing the screen
static const st7789v_init_cmd st7789v_init_commands[] = {
        {0x11, {0},                                                                                  0x80}, // Sleep out
        {0x36, {0},                                                                                  1}, // MADCTL
        {0x3A, {0x05},                                                                               1}, // Maybe 0x55
        {0xB2, {0x0C, 0x0C, 0x00, 0x33, 0x33},                                                       5}, // Porch settings
        {0xB7, {0x75},                                                                               1}, // Gate control
        {0xBB, {0x3D},                                                                               1}, // VCOM setting
        {0xC2, {0x01},                                                                               1}, // VDV and VRH
        {0xC3, {0x19},                                                                               1}, // VRH
        {0xC4, {0x20},                                                                               1}, // VDV
        {0xC6, {0x0F},                                                                               1}, // Frame rate 60Hz, no inversion
        {0xD0, {0xA4, 0xA1},                                                                         2}, // Power control
        {0xE0, {0x70, 0x04, 0x08, 0x09, 0x09, 0x05, 0x2A, 0X33, 0x41, 0x07, 0x13, 0x13, 0x29, 0x2F}, 14}, // Gamma control
        {0XE1, {0x70, 0x03, 0x09, 0x0A, 0x09, 0x06, 0x2B, 0x34, 0x41, 0x07, 0x12, 0x14, 0x28, 0x2E}, 14}, // Gamma control
        {0x21, {0},                                                                                  0x80},
        {0,    {0},                                                                                  0xff}, // End of command
};

/**
 * High level commands
 */
void disp_turn_on() {
    // On command for the display
    st7789v_send_cmd(CMD_DISPLAY_ON);
    vTaskDelay(50 / portTICK_RATE_MS);
}

void disp_turn_off() {
    st7789v_send_cmd(CMD_DISPLAY_OFF);
    vTaskDelay(50 / portTICK_RATE_MS);
}

void disp_set_gamma_control(bool state) {
    st7789v_send_cmd(CMD_GAMMA_CONTROL);
    uint8_t value = 0;

    if (state)
        value = 1 << 2;

    st7789v_send_data(&value, 1);
}

// Sets display rotation, which can be either 0,1,2,3
void disp_set_rotation(uint8_t rot) {
    st7789v_send_cmd(CMD_MADCTL);
    uint8_t madctl = 0;
    switch (rot) {
        case 0:
            // "Natural mode", display facing down
            madctl = 0;
            break;
        case 1:
            // 180°
            madctl = MADCTL_MY | MADCTL_MX;
            break;
        case 2:
            // 270°
            madctl = MADCTL_MV | MADCTL_MH | MADCTL_MX;
            break;
        case 3:
            // 90°
            madctl = MADCTL_MV | MADCTL_MH | MADCTL_MY;
            break;
        default:
            madctl = 0;
            break;
    }

    st7789v_send_data(&madctl, 1);
}

/**********************
 *      MACROS
 **********************/

/**********************
 *   GLOBAL FUNCTIONS
 **********************/
//Send a command to the ILI9341. Uses spi_device_transmit, which waits until the transfer is complete.
void st7789v_send_cmd(const uint8_t cmd) {
    // Wait for other transactions to finish
    while (spi_trans_in_progress) {}

    static spi_transaction_t t;
    memset(&t, 0, sizeof(t));       // Zero out the transaction
    t.length = 9;                   // Command is 9 bits

    uint8_t data[2] = {cmd >> 1, (cmd & 1) << 7};
    //printf("Command sent : %#01x, %#01x, cmd content %#01x \n", data[0], data[1], cmd);
    t.tx_buffer = &data;               // The data is the cmd itself

    spi_trans_in_progress = true;
    spi_device_queue_trans(spi8, &t, portMAX_DELAY);
}

//Send data to the ILI9341. Uses spi_device_transmit, which waits until the transfer is complete.
void st7789v_send_data(const uint8_t *data, int len) {
    // Wait for other transactions to finish
    while (spi_trans_in_progress) {}

    static spi_transaction_t t;
    if (len == 0) return;             //no need to send anything
    memset(&t, 0, sizeof(t));       //Zero out the transaction
    t.length = 9;                 //Len is in bytes, transaction length is in bits.

    // Allocate memory for adding the 9th bit
    for (int i = 0; i < len; i++) {
        uint8_t xt[2] = {(1 << 7) | (*(data + i) >> 1), (*(data + i) & 1) << 7};
        //printf("Data sent : %#01x, %#01x, data content %#01x \n", xt[0], xt[1], data[i]);
        t.tx_buffer = &xt;               //Data
        spi_trans_in_progress = true;
        spi_device_queue_trans(spi8, &t, portMAX_DELAY);
    }
}

//Initialize the display
void st7789v_init(void) {
    // Init of SPI
    esp_err_t ret;

    //Initialize the SPI bus
    ret = spi_bus_initialize(HSPI_HOST, &buscfg, 1);
    assert(ret == ESP_OK);

    //Attach the LCD to the SPI bus
    ret = spi_bus_add_device(HSPI_HOST, &devcfg8bits, &spi8);
    assert(ret == ESP_OK);

    //Initialize non-SPI GPIOs
    gpio_set_direction(PIN_NUM_RST, GPIO_MODE_OUTPUT);
    gpio_set_direction(PIN_NUM_BCKL, GPIO_MODE_OUTPUT);

    //Reset the display
    gpio_set_level(PIN_NUM_RST, 0);
    vTaskDelay(100 / portTICK_RATE_MS);
    gpio_set_level(PIN_NUM_RST, 1);
    vTaskDelay(100 / portTICK_RATE_MS);

    int cmd = 0;

    //Send all the commands
    while (st7789v_init_commands[cmd].databytes != 0xff) {
        st7789v_send_cmd(st7789v_init_commands[cmd].cmd);
        st7789v_send_data(st7789v_init_commands[cmd].data, st7789v_init_commands[cmd].databytes & 0x1F);
        if (st7789v_init_commands[cmd].databytes & 0x80) {
            vTaskDelay(100 / portTICK_RATE_MS);
        }
        cmd++;
    }

    ///Enable backlight
    //gpio_set_level(PIN_NUM_BCKL, 0);
}

void st7789v_flush(lv_disp_drv_t *drv, const lv_area_t *area, lv_color_t *color_map) {
    uint8_t data[4];

    /*Column addresses*/
    st7789v_send_cmd(0x2A);
    data[0] = (area->x1 >> 8) & 0xFF;
    data[1] = area->x1 & 0xFF;
    data[2] = (area->x2 >> 8) & 0xFF;
    data[3] = area->x2 & 0xFF;
    st7789v_send_data(data, 4);

    /*Page addresses*/
    st7789v_send_cmd(0x2B);
    data[0] = (area->y1 >> 8) & 0xFF;
    data[1] = area->y1 & 0xFF;
    data[2] = (area->y2 >> 8) & 0xFF;
    data[3] = area->y2 & 0xFF;
    st7789v_send_data(data, 4);

    /*Memory write*/
    st7789v_send_cmd(0x2C);
    uint16_t size = lv_area_get_size(area); // Number of pixels
    st7789v_send_color((void *) color_map, size);
}

uint8_t *buffer = NULL;

/**********************
 *   STATIC FUNCTIONS
 **********************/
// Here length is the number of pixels (each taking 16 bits)
static void st7789v_send_color(void *data, uint16_t length) {
    //Transaction descriptors. Declared static so they're not allocated on the stack; we need this memory even when this
    //function is finished because the SPI driver needs access to it even while we're already calculating the next line.
    static spi_transaction_t trans;

    // Wait for other transactions to finish
    while (spi_trans_in_progress) {}

    memset(&trans, 0, sizeof(spi_transaction_t));
    trans.flags = 0;
    trans.length = length * 18;

    // Buffer for a given size of pixels, each having 18 bits (16 bits colors + 2 bits DC / pixel)
    buffer = malloc((length * 18) / 8);
    assert(buffer != NULL);
    trans.tx_buffer = buffer;

    // Packs pixels into 9 bit format (with leading DC bit), this is a bit the mess
    // but required as the driver does not support 9 bit mode. We first send all the pixels
    // that fit in a multiple of 4, and we send the remaining pixels afterwards if required.
    for (int i = 0; i < length / 4; i++) {
        uint16_t p0 = ((uint16_t *) data)[0 + i * 4];
        uint16_t p1 = ((uint16_t *) data)[1 + i * 4];
        uint16_t p2 = ((uint16_t *) data)[2 + i * 4];
        uint16_t p3 = ((uint16_t *) data)[3 + i * 4];
        // Prepare 4 pixels for sending, which can be packed with 72 bits (aka 8 bytes)
        *(buffer + 0 + i * 9) = 1 << 7 | (p0 >> 9);
        *(buffer + 1 + i * 9) = 1 << 6 | ((p0 >> 2) & 0b111111) | (((p0 >> 8) & 1) << 7);
        *(buffer + 2 + i * 9) = 1 << 5 | ((p0 & 0b11) << 6) | ((p1 >> 11) & 0b11111);
        *(buffer + 3 + i * 9) = 1 << 4 | (((p1 >> 8) & 0b111) << 5) | ((p1 >> 4) & 0b1111);
        *(buffer + 4 + i * 9) = 1 << 3 | ((p1 & 0b1111) << 4) | ((p2 >> 13));
        *(buffer + 5 + i * 9) = 1 << 2 | (((p2 >> 8) & 0b11111) << 3) | ((p2 >> 6) & 0b11);
        *(buffer + 6 + i * 9) = 1 << 1 | (((p2 & 0b111111) << 2)) | (((p3 >> 15)));
        *(buffer + 7 + i * 9) = 1 << 0 | (((p3 >> 8) & 0x7f) << 1);
        *(buffer + 8 + i * 9) = p3 & 0xff;
    }

    // Number of pixels remaining to be sent after having sent length / 4 pixel
    uint16_t offset = length / 4;
    uint8_t rest = length - offset;

    uint16_t p0, p1, p2;

    switch (rest) {
        case 1: {
            uint16_t p0 = ((uint16_t *) data)[0 + offset * 4];

            // Prepare 1 pixels for sending
            *(buffer + 0 + offset * 9) = 1 << 7 | (p0 >> 9);
            *(buffer + 1 + offset * 9) = 1 << 6 | ((p0 >> 2) & 0b111111) | (((p0 >> 8) & 1) << 7);
            *(buffer + 2 + offset * 9) = 1 << 5 | ((p0 & 0b11) << 6);
            break;
        }
        case 2 : {
            p0 = ((uint16_t *) data)[0 + offset * 4];
            p1 = ((uint16_t *) data)[1 + offset * 4];
            // Prepare 2 pixels for sending
            *(buffer + 0 + offset * 9) = 1 << 7 | (p0 >> 9);
            *(buffer + 1 + offset * 9) = 1 << 6 | ((p0 >> 2) & 0b111111) | (((p0 >> 8) & 1) << 7);
            *(buffer + 2 + offset * 9) = 1 << 5 | ((p0 & 0b11) << 6) | ((p1 >> 11) & 0b11111);
            *(buffer + 3 + offset * 9) = 1 << 4 | (((p1 >> 8) & 0b111) << 5) | ((p1 >> 4) & 0b1111);
            *(buffer + 4 + offset * 9) = 1 << 3 | ((p1 & 0b1111) << 4);
            break;
        }
        case 3 : {
            p0 = ((uint16_t *) data)[0 + offset * 4];
            p1 = ((uint16_t *) data)[1 + offset * 4];
            p2 = ((uint16_t *) data)[2 + offset * 4];
            // Prepare 3 pixels for sending
            *(buffer + 0 + offset * 9) = 1 << 7 | (p0 >> 9);
            *(buffer + 1 + offset * 9) = 1 << 6 | ((p0 >> 2) & 0b111111) | (((p0 >> 8) & 1) << 7);
            *(buffer + 2 + offset * 9) = 1 << 5 | ((p0 & 0b11) << 6) | ((p1 >> 11) & 0b11111);
            *(buffer + 3 + offset * 9) = 1 << 4 | (((p1 >> 8) & 0b111) << 5) | ((p1 >> 4) & 0b1111);
            *(buffer + 4 + offset * 9) = 1 << 3 | ((p1 & 0b1111) << 4) | ((p2 >> 13));
            *(buffer + 5 + offset * 9) = 1 << 2 | (((p2 >> 8) & 0b11111) << 3) | ((p2 >> 6) & 0b11);
            *(buffer + 6 + offset * 9) = 1 << 1 | (((p2 & 0b111111) << 2));
            break;
        }
    }

    spi_trans_in_progress = true;
    spi_color_sent = true;
    spi_device_queue_trans(spi8, &trans, portMAX_DELAY);
}

// Call-back when a SPI transaction terminates
static void IRAM_ATTR spi_ready(spi_transaction_t *trans) {
    spi_trans_in_progress = false;

    // If we just sent color lines, free the buffer and tell the
    // library it can send other lines if required
    if (spi_color_sent) {
        free(buffer);
        lv_disp_t *disp = lv_refr_get_disp_refreshing();
        lv_disp_flush_ready(&disp->driver);
        spi_color_sent = false;
    }
}