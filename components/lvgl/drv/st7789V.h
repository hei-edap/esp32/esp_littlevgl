/**
 * @file lv_templ.h
 *
 */

#ifndef ST7789V_H
#define ST7789V_H

#ifdef __cplusplus
extern "C" {
#endif

/*********************
 *      INCLUDES
 *********************/
#include "../lvgl.h"

/*********************
 *      DEFINES
 *********************/
#define N_LINES_SENT 24
#define DISP_BUF_SIZE (LV_HOR_RES_MAX * N_LINES_SENT)

#define LOW_SPEED  26 * 1000000
#define HIGH_SPEED 40 * 1000000
#define SPI_SPEED HIGH_SPEED

#define PIN_NUM_MOSI 21
#define PIN_NUM_CLK  19
#define PIN_NUM_CS   22
#define PIN_NUM_RST  18

#define PIN_NUM_BCKL 5 // Not used
#define PIN_NUM_MISO -1 // Not used

// LCD Commands
#define CMD_DISPLAY_OFF 0x28
#define CMD_DISPLAY_ON  0x29
#define CMD_GAMMA_CONTROL 0xba
#define CMD_MADCTL 0x36

#define MADCTL_MY  0x80
#define MADCTL_MX  0x40
#define MADCTL_MV  0x20
#define MADCTL_ML  0x10
#define MADCTL_MH  0x04

/**********************
 *      TYPEDEFS
 **********************/


/**********************
 * GLOBAL PROTOTYPES
 **********************/
void st7789v_init(void);
void st7789v_flush(lv_disp_drv_t * drv, const lv_area_t * area, lv_color_t * color_map);
void st7789v_round(lv_disp_drv_t * dispDrv, lv_area_t * area);

/**
 * High level commands
 */
void disp_turn_on();
void disp_turn_off();
void disp_set_gamma_control(bool state);
void disp_set_rotation(uint8_t rot); // Can be 0, 1, 2, 3

/**********************
 *      MACROS
 **********************/


#ifdef __cplusplus
} /* extern "C" */
#endif

#endif
