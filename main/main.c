#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <freertos/FreeRTOS.h>
#include <freertos/task.h>

#include "esp_system.h"
#include "esp_freertos_hooks.h"
#include "esp_log.h"
#include "esp_system.h"
#include <esp_pm.h>
#include <esp32/clk.h>
#include "esp_spi_flash.h"

#include "driver/gpio.h"

#include <lvgl.h>
#include <drv/st7789V.h>
#include <src/lv_core/lv_style.h>

#include "lv_tests/lv_test_stress/lv_test_stress.h"
#include "lv_tests/lv_test_misc/lv_test_task.h"
#include "lv_tests/lv_test_theme/lv_test_theme_1.h"
#include "lv_tests/lv_test_theme/lv_test_theme_2.h"

#include "lv_tutorial/1_hello_world/lv_tutorial_hello_world.h"
#include "lv_tutorial/2_objects/lv_tutorial_objects.h"
#include "lv_tutorial/3_styles/lv_tutorial_styles.h"
#include "lv_tutorial/4_themes/lv_tutorial_themes.h"
#include "lv_tutorial/5_antialiasing/lv_tutorial_antialiasing.h"
#include "lv_tutorial/6_images/lv_tutorial_images.h"
#include "lv_tutorial/7_fonts/lv_tutorial_fonts.h"
#include "lv_tutorial/8_animations/lv_tutorial_animations.h"

static const char * TAG = "LittleVGL testing";

static void IRAM_ATTR lv_tick_task(void);

static lv_color_t buf1[DISP_BUF_SIZE];
static lv_color_t buf2[DISP_BUF_SIZE];
static lv_disp_buf_t disp_buf;

/*Declare the "source code image" which is stored in the flash*/
LV_IMG_DECLARE(red_rose_16);
LV_IMG_DECLARE(pouet);

lv_obj_t *gauge1;
lv_obj_t *firemonLogoImage;

void test_gauge() {
    /*Create a style*/
    static lv_style_t style;
    lv_style_copy(&style, &lv_style_pretty_color);
    style.body.main_color = LV_COLOR_RED;//lv_color_hex3(0x666); /*Line color at the beginning*/
    style.body.grad_color = LV_COLOR_RED;//lv_color_hex3(0x666); /*Line color at the end*/
    style.body.padding.left = 8; /*Scale line length*/
    style.body.padding.inner = 15; /*Scale label padding*/
    style.body.radius = 7;
    style.body.border.color = lv_color_hex3(0x333); /*Needle middle circle color*/
    style.line.width = 3;
    style.text.color = lv_color_hex3(0x333);
    style.text.font = &lv_font_roboto_22;
    style.line.color = LV_COLOR_BLACK; /*Line color after the critical value*/
    /*Describe the color for the needles*/
    static lv_color_t needle_colors[] = {LV_COLOR_BLUE};
    /*Create a gauge*/
    gauge1 = lv_gauge_create(lv_scr_act(), NULL);
    lv_gauge_set_style(gauge1, LV_GAUGE_STYLE_MAIN, &style);
    lv_gauge_set_needle_count(gauge1, 1, needle_colors);
    lv_gauge_set_range(gauge1, 0, 350);
    lv_gauge_set_critical_value(gauge1, 100);
    //lv_gauge_set_scale(gauge1, 180, 17, 6);
    lv_obj_set_size(gauge1, 240, 240);
    lv_obj_align(gauge1, NULL, LV_ALIGN_CENTER, 0, 0);
    /*Set the values*/
    lv_gauge_set_value(gauge1, 0, 10);
}


void init_graphics() {
    // Init LittleVGL
    lv_init();

    // Init display
    st7789v_init();

    lv_disp_buf_init(&disp_buf, buf1, buf2, DISP_BUF_SIZE);
    lv_disp_drv_t disp_drv;
    lv_disp_drv_init(&disp_drv);

    disp_drv.flush_cb = st7789v_flush;
    disp_drv.buffer = &disp_buf;

    // Even though screen is only 240x240, y coordinate is in the middle of the controlled address
    disp_drv.ver_res = 320;

    lv_disp_drv_register(&disp_drv);
    lv_obj_t * scr = lv_obj_create(NULL, NULL);
    lv_scr_load(scr);
}

// Called when the startup animation is over
void startup_image_animation_over(){
    ESP_LOGI(TAG, "Animation finished\n");
}

void image_startup() {
    lv_obj_t *scr = lv_disp_get_scr_act(NULL);     /*Get the current screen*/

    firemonLogoImage = lv_img_create(scr, NULL); /*Crate an image object*/
    lv_img_set_src(firemonLogoImage, &red_rose_16);  /*Set the created file as image (a red rose)*/
    lv_obj_set_pos(firemonLogoImage, 0, 40);      /*Set the positions*/

    static lv_style_t style_default;

//    lv_obj_t *img_var2;
//    img_var2 = lv_img_create(scr, NULL); /*Crate an image object*/
//    lv_img_set_src(img_var2, &pouet);  /*Set the created file as image (a red rose)*/
//    lv_obj_set_pos(img_var2, 0, 40);      /*Set the positions*/

    static lv_style_t style_opaque_begin, style_opaque_end;
    lv_style_copy(&style_opaque_begin, &style_default);
    lv_style_copy(&style_opaque_end, &style_default);

    style_opaque_begin.image.opa = 255;
    style_opaque_end.image.opa = 0;

    lv_img_set_style(firemonLogoImage, LV_IMG_STYLE_MAIN, &style_default);
//    lv_img_set_style(img_var2, LV_IMG_STYLE_MAIN, &style_default);

    lv_anim_t a;
    lv_style_anim_init(&a);
    lv_style_anim_set_styles(&a, &style_default, &style_opaque_begin, &style_opaque_end);
    lv_style_anim_set_time(&a, 800, 2200);
    lv_style_anim_set_ready_cb(&a, startup_image_animation_over);
    lv_style_anim_create(&a);
}

// Lock to acquire to go to full speed, otherwise 40 MHz is used instead
esp_pm_lock_handle_t full_speed;

void init_dfs(){

    // Enable power management with dynamic frequency scaling
    #if !CONFIG_PM_ENABLE
        #error "CONFIG_PM_ENABLE missing for DFS"
    #endif
    
     #if !CONFIG_FREERTOS_USE_TICKLESS_IDLE
         #error "CONFIG_FREERTOS_USE_TICKLESS_IDLE missing for DFS"
     #endif

//     esp_pm_config_esp32_t config = {
//         .light_sleep_enable = false,
//         .max_freq_mhz = 240,
//         .min_freq_mhz = 80,
//     };
//
//    esp_err_t err = esp_pm_configure(&config);
//
//    if(err != ESP_OK){
//        if(err == ESP_ERR_INVALID_ARG)
//            ESP_LOGE("DFS", "Problem while configuring DFS, invalid argument");
//        if(err == ESP_ERR_NOT_SUPPORTED)
//            ESP_LOGE("DFS", "Problem while configuring DFS, not supported");
//    }

    // Create a lock with a certain power management parameter    
    ESP_ERROR_CHECK(esp_pm_lock_create(ESP_PM_CPU_FREQ_MAX, 0, "240 MHz lock", &full_speed));
}

void display_cpu_freq(){
    static rtc_cpu_freq_config_t cpu;
    rtc_clk_cpu_freq_get_config(&cpu);
    ESP_LOGI("DFS", "CPU freq %d", cpu.freq_mhz);
}


void app_header(){
    ESP_LOGI(TAG, "************************************************");
    ESP_LOGI(TAG, "*** ESP_LittleVGL_Example - PMR-Systems 2019 ***");
    ESP_LOGI(TAG, "************************************************");    
    ESP_LOGI(TAG, "\tBuild date %s - %s", __DATE__, __TIME__);

    esp_chip_info_t chip_info;
    esp_chip_info(&chip_info);

    ESP_LOGI(TAG, "\tUsing ESP32 chip with %d CPU cores, WiFi%s%s, ",
           chip_info.cores,
           (chip_info.features & CHIP_FEATURE_BT) ? "/BT" : "",
           (chip_info.features & CHIP_FEATURE_BLE) ? "/BLE" : "");
    ESP_LOGI(TAG, "Silicon revision %d,", chip_info.revision);
    ESP_LOGI(TAG, "%dMB %s flash", spi_flash_get_chip_size() / (1024 * 1024), (chip_info.features & CHIP_FEATURE_EMB_FLASH) ? "embedded" : "external");
}

void app_main() {
    app_header();
    init_dfs();
    display_cpu_freq();
    esp_register_freertos_tick_hook(lv_tick_task);
    ESP_ERROR_CHECK(esp_pm_lock_acquire(full_speed));

    init_graphics();

    // display_cpu_freq();
    //ESP_ERROR_CHECK(esp_pm_lock_release(full_speed));

    // TODO this one has a memory error
    //lv_test_stress_1(); //
    //lv_test_theme_2();

    // TODO 90 and 270 rotation not completely done, as I also have to turn resolution in LittleVGL
    disp_set_rotation(1); // Can be 0, 1, 2, 3
    test_gauge();
    image_startup();
    disp_turn_on();

    //lv_tutorial_hello_world();
    //lv_tutorial_antialiasing();
    //lv_tutorial_objects();
    //lv_tutorial_fonts();
    //lv_tutorial_animations();

    int direction = 1;
    int value = 10;    

    char txt[500];

    esp_pm_dump_locks(stdout);

    while (1) {
        value += direction;
        
        if (value == 0 || value == 350) {
            direction *= -1;
        }

        lv_gauge_set_value(gauge1, 0, value);
        lv_task_handler();
        vTaskDelay(10 / portTICK_PERIOD_MS);
    }
}

// display_cpu_freq();
// esp_pm_dump_locks(stdout);

// vTaskList(txt);
// printf("Task  State   Prio    Stack    Num\n");
// printf("%s", txt);

static void IRAM_ATTR lv_tick_task(void) {
    lv_tick_inc(portTICK_RATE_MS);
}
